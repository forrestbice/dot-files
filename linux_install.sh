echo "Installing Packages...\n\n"

echo "Installing Zsh..\n"
sudo apt-get install zsh

echo "Setting Zsh as default shell..\n"
chsh -s $(which zsh)

echo "Installing Fzf..\n"
sudo apt-get install fzf
/usr/local/opt/fzf/install

echo "Installing Ack..\n"
sudo apt-get install ack

echo "\nInstalling Tmux...\n"
sudo apt-get install tmux

echo "\nInstalling Python 3...\n"
sudo apt-get install tmux

echo "\nInstalling Pidcat...\n"
sudo apt-get install pidcat

echo "\nInstalling Git LFS...\n"
sudo apt-get install git-lfs

echo "\nInstalling SDKMan...\n"
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"

echo "\nInstalling Kotlin...\n"
sdk install kotlin

echo "\nInstalling KScript...\n"
sdk install kscript

echo "\nInstalling Bazel...\n"
echo "deb [arch=amd64] https://storage.googleapis.com/bazel-apt stable jdk1.8" | sudo tee /etc/apt/sources.list.d/bazel.list
sudo apt-get install curl
curl https://bazel.build/bazel-release.pub.gpg | sudo apt-key add -
sudo apt-get update && sudo apt-get install bazel

echo "Installing Dot Files...\n"
./install.sh
