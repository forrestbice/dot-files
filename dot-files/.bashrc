# Source git helper files
source "$HOME/.git-prompt.sh"
source "$HOME/.git-completion.bash"
[[ -s "$HOME/.forge_android_environment" ]] && source "$HOME/.forge_android_environment"

# Setup the prompt
export GIT_PS1_SHOWDIRTYSTATE=true
export GIT_PS1_SHOWSTASHSTATE=true
export GIT_PS1_SHOWUNTRACKEDFILES=true

# Prompt Formatting
export PS1="\[\033[00m\]\u@\h\[\033[01;34m\] \W \[\033[31m\]\$(__git_ps1) \[\033[00m\]$\[\033[00m\] "

# Show current git branch
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# Append to the history file, don't overwrite it
shopt -s histappend

# Check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# Source Aliases if they exist
[[ -f "$HOME/.aliases" ]] && source "$HOME/.aliases"
[[ -f "$HOME/.localaliases" ]] && source "$HOME/.localaliases"

#Custom Aliases & Functions:

    #IP Address
    alias ip="ifconfig | grep 'inet '"
    alias whats-my-ip="ifconfig | grep -B 6 ' active'"

    #Listing Files
    alias l='ls -CF'
    alias ls='ls -G'
    alias la='ls -lha'
    # Or Alternatively: alias ls='ls --color=auto'

    #Directories
    function cdl() { cd $1 && ls; }

    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    #Git
      #Add
      alias gap='git add -p'
      alias gaa='git add *'

      #Branch
      alias gb='git branch'

      #Checkout
      alias gc='git checkout'
      alias gcb='git checkout -b'

      #Commit
      alias gco='git commit'
      alias gca='git commit -a'

      #Diff
      alias gd='git diff'
      alias gdh='git diff HEAD'
      alias gdm='git diff origin/master'

      #Log
      alias gl='git log --name-status'
      alias glm='git log --name-status --author="$USER"'
      alias glv='git glv'

      #Other
      alias gs='git status'
      alias gf='git fetch'
      alias gm='git merge'
      alias gsl='git stash list'
      alias gsu='git submodule update'
      alias gcp='git cherry-pick'

      # See https://hackernoon.com/lesser-known-git-commands-151a1918a60
      git config --global alias.please 'push --force-with-lease'
      git config --global alias.commend 'commit --amend --no-edit'

    #Grep
    alias gr='grep -r'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'

    #Find
    alias f='find . -iname'

    # Most Used Commands
    # See http://lifehacker.com/software/how-to/turbocharge-your-terminal-274317.php for more info
    alias profileme="history | awk '{print \$4}' | awk 'BEGIN{FS=\"|\"}{print \$1}' | sort | uniq -c | sort -n | tail -n 20 | sort -nr"

    #C
    function gccw() {
      gcc -Wall -Wextra -Werror -Wfloat-equal -Wundef -Wshadow \
      -Wpointer-arith -Wcast-align -Wstrict-prototypes -Wmissing-prototypes \
      -Wstrict-overflow=5 -Wwrite-strings -Waggregate-return -Wcast-qual \
      -Wswitch-default -Wswitch-enum -Wconversion -Wunreachable-code \
      -Wparentheses $1;
    }

    function bashrc {
      $EDITOR ~/.bash_profile && source ~/.bash_profile
    }

    function profile {
      $EDITOR ~/.bash_profile && source ~/.bash_profile
    }

# Source The Environment if it exists
[ -e ~/.env ] && source ~/.env

# This loads nvm
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
# This loads nvm bash_completion
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

# Fzf
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
