"NeoBundle Scripts-----------------------------

" Note: Skip initialization for vim-tiny or vim-small
if 0 | endif

if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=$HOME/.vim/bundle/neobundle.vim/

" Required:
call neobundle#begin(expand('$HOME/.vim/bundle'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" Add or remove your Bundles here:
" Disabling 'pre-installed' bundles
" NeoBundle 'Shougo/neosnippet.vim'
" NeoBundle 'Shougo/neosnippet-snippets'

NeoBundle 'kien/ctrlp.vim'
NeoBundle 'tpope/vim-rails'
NeoBundle 'tpope/vim-haml'
NeoBundle 'tpope/vim-bundler'
NeoBundle 'tpope/vim-fugitive'
NeoBundle 'ctrlpvim/ctrlp.vim'
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'flazz/vim-colorschemes'
NeoBundle 'kien/rainbow_parentheses.vim'


" You can specify revision/branch/tag.
NeoBundle 'Shougo/vimshell', { 'rev' : '3787e5' }

" Required:
call neobundle#end()

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck

"End NeoBundle Scripts-------------------------

syntax on               " Syntax highlighting
syntax enable           " Syntax highlighting

color slate             " Set color

set expandtab           " Use spaces instead of tabs
set autoindent          " Automatically indent
set tabstop=2           " Tab = 2
set shiftwidth=2        " Indents are 2 spaces
set softtabstop=2       " 2 spaces are inserted when tab is pressed
set clipboard=unnamed   " Allow system clipboard

set spell               " Enable spell check
set spelllang=en_us     " Specify the spell region
set number              " Display line numbers
set title               " Display editing filename
set ruler               " Display line & column numbers

set background=dark     " Enable a dark background
set mouse=a             " Enable scrolling with mouse

" Map kk to 'Esc'
imap kk <ESC>

" Remap spelling
map m ]s
map <S-m> [s
map <S-m> [s
map fm z=

" Remove trailing spaces etc.
function RemoveTrailing()
  :%s/\s\+$//
endfunction

" Format JSON
com! FormatJson %!python -m json.tool

" Racket / Lisp filetype
if has("autocmd")
  au BufReadPost *.rkt,*.rktl set filetype=scheme
endif

" Turn on Rainbow Parenthesis
au VimEnter * RainbowParenthesesToggle

" Add column limit
if exists('+colorcolumn')
  set colorcolumn=80
else
  au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
endif

" Match and highlight extra white spaces
highlight ExtraWhitespace ctermbg=red guibg=red

" Eliminate any trailing whitespace on write
autocmd BufWritePre * :%s/\s\+$//e

autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

match ExtraWhitespace /\s\+$/
