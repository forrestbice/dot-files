# Load ~/.bashrc
[ -r ~/.bashrc ] && source ~/.bashrc

# Load RVM into a shell session *as a function*
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/Users/bice/.sdkman"
[[ -s "/Users/bice/.sdkman/bin/sdkman-init.sh" ]] && source "/Users/bice/.sdkman/bin/sdkman-init.sh"
