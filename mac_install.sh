echo "Installing Homebrew\n\n"
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

echo "\nInstalling Homebrew Packages...\n\n"

echo "Installing Zsh...\n"
brew install zsh zsh-completions

echo "\nSetting Zsh as default shell...\n"
echo "$(which zsh)" | sudo tee -a /etc/shells
chsh -s $(which zsh)

echo "\nInstalling Vim...\n"
brew install vim

echo "\nInstalling CoreUtils...\n"
brew install coreutils

echo "\nInstalling Fzf...\n"
brew install fzf
/usr/local/opt/fzf/install

echo "\nInstalling Ack...\n"
brew install ack

echo "\nInstalling Tmux...\n"
brew install tmux

echo "\nInstalling Python...\n"
brew install python

echo "\nInstalling Pidcat...\n"
brew install pidcat

echo "\nInstalling Bat...\n"
brew install bat

echo "\nInstalling Tree...\n"
brew install tree

echo "\nInstalling GeoIP...\n"
brew install geoip

echo "\nInstalling Links...\n"
brew install links

echo "\nInstalling Cask...\n"
brew install cask

echo "\nInstalling Git Quick Stats...\n"
brew install git-quick-stats

echo "\nInstalling Git LFS...\n"
brew install git-lfs

echo "\nInstalling Spectacle...\n"
brew cask install spectacle

echo "\nInstalling CheatSheet...\n"
brew cask install cheatsheet

echo "\nInstalling Dropbox...\n"
brew cask install dropbox

echo "\nInstalling Google Chrome...\n"
brew cask install google-chrome

echo "\nInstalling iTerm2...\n"
brew cask install iterm2

echo "\nInstalling Kotlin...\n"
brew install kotlin

echo "\nInstalling KScript...\n"
brew install holgerbrandl/tap/kscript

echo "\nInstalling Bazel...\n"
brew tap bazelbuild/tap
brew install bazelbuild/tap/bazel

echo "\nInstalling Youtube-DL...\n"
brew install youtube-dl

echo "\nInstalling iTerm2 Preferences...\n"
LINK_SOURCE_PATH=$(realpath "iTerm/settings/com.googlecode.iterm2.plist")
ln -s $LINK_SOURCE_PATH ~/Library/Preferences

echo "\nInstalling iTerm2 Dynamic Profiles...\n"
LINK_SOURCE_PATH=$(realpath "iTerm/profiles/profiles.json")
ln -s $LINK_SOURCE_PATH ~/Library/Application\ Support/iTerm2/DynamicProfiles

echo "\nInstalling Dot Files...\n"
./install.sh
