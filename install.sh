echo "Installing NeoBundle for Vim...\n\n"
curl https://raw.githubusercontent.com/Shougo/neobundle.vim/master/bin/install.sh | sh
echo "\nNeoBundle for Vim install complete!\n\n"

echo "Installing Zsh...\n\n"
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
echo "\nZsh install complete!\n\n"

echo "Installing External Zsh Plugins...\n"
git clone git://github.com/gradle/gradle-completion ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/gradle-completion
git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-completions
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
echo "\nExternal Zsh Plugins Installed!\n\n"

echo "Installing Zsh Theme...\n"
FORREST_THEME_SOURCE_PATH=$(realpath "zsh-theme/forrest.zsh-theme")
FORREST_THEME_DESTINATION_PATH=${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/forrest.zsh-theme
echo "Creating theme symlink from $FORREST_THEME_SOURCE_PATH to $FORREST_THEME_DESTINATION_PATH"
ln -s $FORREST_THEME_SOURCE_PATH $FORREST_THEME_DESTINATION_PATH
echo "\nZsh Theme Installed!\n\n"

echo "Installing dot files...\n"

FORMATTED_TIME=$(date '+%Y-%m-%d-%H-%M-%S')
BACKUP_DIR=$HOME/dot-file-backup-$FORMATTED_TIME
if [ -d $BACKUP_DIR ]; then
  echo "Backup directory $BACKUP_DIR already exists\n"
else
  echo "Creating a backup directory $BACKUP_DIR\n"
  mkdir $BACKUP_DIR
fi

DOT_FILES_DIR=dot-files/.*
for filename in $DOT_FILES_DIR; do
  if [ -f $filename ]; then
    FILE_NAME=$(basename $filename)
    LINK_SOURCE_PATH=$(realpath "$filename")
    LINK_DESTINATION_PATH=$HOME/$FILE_NAME
    if [ -f $LINK_DESTINATION_PATH ] || [ -L $LINK_DESTINATION_PATH ]; then
      echo "Moving existing $FILE_NAME to $BACKUP_DIR/$FILE_NAME"
      $(mv $LINK_DESTINATION_PATH $BACKUP_DIR)
    else
      echo "$FILE_NAME does not currently exist"
    fi
    echo "Creating $FILE_NAME symlink from $LINK_SOURCE_PATH to $LINK_DESTINATION_PATH\n"
    ln -s $LINK_SOURCE_PATH $LINK_DESTINATION_PATH
  fi
done

echo "Dot file install complete!"
